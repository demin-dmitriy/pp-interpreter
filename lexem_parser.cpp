#include "lexem_parser.h"
#include <cctype>

namespace
{
    template<int (*T)(int)>
    bool cast_func_arg(char ch)
    {
        return T(ch);
    }

    bool is_comment(char ch)
    {
        return ch == '#';
    }

    bool is_cmp_operator(char ch)
    {
        switch (ch)
        {
            default: return false;
            case '<': case '>': case '=': case'&': case '!': return true;
        }
    }
}

lexem_parser_t::lexem_parser_t(std::istream &stream)
    : stream_(stream)
    , line_number_(0)
    , no_new_line_(false)
{
    skip_non_tokens();
    next_line();
}

bool lexem_parser_t::next_char(char& ch)
{
    if (no_new_line_)
    {
        return false;
    }
    ch = stream_.get();
    if (stream_ && ch == '\n')
    {
        no_new_line_ = true;
        return false;
    }
    return stream_;
}

void lexem_parser_t::skip_while(bool(*pred)(char))
{
    for (char ch; pred(stream_.peek()) && next_char(ch);)
    {
    }
}

void lexem_parser_t::accumulate_while(std::string& str, bool(*pred)(char))
{
    for (char ch; pred(stream_.peek()) && next_char(ch);)
    {
        str.push_back(ch);
    }
}

std::string lexem_parser_t::next()
{
    peek();
    std::string res;
    res.swap(next_str_);
    return res;
}

void lexem_parser_t::next_line()
{
    while (no_new_line_ == true)
    {
        ++line_number_;
        no_new_line_ = false;
        skip_non_tokens();
    }
}

void lexem_parser_t::skip_non_tokens()
{
    skip_while(cast_func_arg<isspace>);
    if (is_comment(stream_.peek()))
    {
        skip_while([](char ch){return true;}); // Skip till the end of the line.
    }
}

const std::string &lexem_parser_t::peek()
{
    if (next_str_.size() != 0)
    {
        return next_str_;
    }
    skip_non_tokens();
    char ch;
    if (next_char(ch))
    {
        next_str_.push_back(ch);
        if (isalpha(ch))
        {
            accumulate_while(next_str_, cast_func_arg<isalnum>);
        }
        else if (isdigit(ch))
        {
            accumulate_while(next_str_, cast_func_arg<isdigit>);
        }
        else if (is_cmp_operator(ch))
        {
            accumulate_while(next_str_, is_cmp_operator);
        }
    }
    return next_str_;
}

int lexem_parser_t::line_number() const
{
    return line_number_;
}
