#ifndef VARIABLE_HANDLER_H
#define VARIABLE_HANDLER_H

#include <unordered_map>
#include <string>

struct variable_manager_t
{
    size_t get_var_index(std::string const& str)
    {
        auto element = var_indexes_.find(str);
        if (element == var_indexes_.end())
        {
            size_t n = var_indexes_.size();
            return var_indexes_[str] = n;
        }
        return element->second;
    }

    size_t amount()
    {
        return var_indexes_.size();
    }

private:
    std::unordered_map<std::string, int> var_indexes_;
};

#endif // VARIABLE_HANDLER_H
