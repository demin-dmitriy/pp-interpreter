#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "lexem_parser.h"
#include "variable_manager.h"

#include <cstdint>
#include <exception>
#include <stack>
#include <string>
#include <vector>
#include <deque>

typedef int32_t number_t; // Type for PP language variables.

class parse_exception: public std::exception
{
    std::string description_;
public:
    parse_exception(std::string const& description_);
    virtual char const* what() const noexcept;
    virtual ~parse_exception() noexcept;
};

class division_by_zero_exception: public std::exception
{
public:
    virtual char const* what() const noexcept;
    virtual ~division_by_zero_exception() noexcept;
};

struct expression_t
{
    struct token_t
    {
        enum type_t
        {
            constant,
            variable,
            operation
        } type;
        union get_t
        {
            number_t constant;
            std::size_t variable;
            enum operation_t
            {
                plus,
                minus,
                // un_plus,
                un_minus,
                division,
                multiplication,
                opening_bracket, // Should not occure in operations_.
                closing_bracket  // Should not occure in operations_.
            } operation;
        } get;
        typedef get_t::operation_t operation_t;
        token_t(type_t type, int val);
        token_t(operation_t op);
    };

    // Should be run only once.
    void parse(lexem_parser_t& input, variable_manager_t& vars);
    number_t eval(std::vector<number_t> const&) const;

private:
    typedef token_t::operation_t op_t;
    // Expression converted to and stored in reverse polish notation.
    std::vector<token_t> operations_;
    void aux_parse(lexem_parser_t& input, variable_manager_t& vars);
    void add_operator(op_t op, std::stack<op_t> &op_stack);
};

#endif
