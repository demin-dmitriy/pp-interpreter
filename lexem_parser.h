#ifndef LEXEM_PARSER_H
#define LEXEM_PARSER_H

#include <istream>
#include <string>

struct lexem_parser_t
{
    std::string next();
    void next_line();
    std::string const& peek();
    int line_number() const;
    lexem_parser_t(std::istream&);

private:
    void accumulate_while(std::string&, bool (*pred)(char));
    void skip_while(bool (*pred)(char));
    void ignore_till(char ch);
    bool next_char(char& ch);
    void skip_non_tokens();
    std::istream& stream_;
    std::string next_str_; // Cache for efficient peek.
    int line_number_;
    bool no_new_line_; // Should be explicitly unset by next_line()
                       // to read from new_line.
};

#endif
