#include "expression.h"

#include <cassert>
#include <cctype>
#include <cstdlib>
#include <string>

typedef expression_t::token_t::operation_t op_t;

namespace
{
    number_t aton(std::string const& str)
    {
        return atoi(str.c_str());
    }

    int priority(expression_t::token_t::operation_t op)
    {
        switch (op)
        {
            case op_t::plus:
            case op_t::minus: return 0;
            case op_t::division:
            case op_t::multiplication: return 1;
            case op_t::un_minus: return 2;
            case op_t::opening_bracket:
            case op_t::closing_bracket: return -1;
        }
        assert(false);
        return 3;
    }

    void process_operation(std::stack<number_t>& values, op_t op)
    {
        if (op == op_t::un_minus)
        {
            number_t a = values.top();
            values.pop();
            values.push(-a);
            return;
        }

        number_t a = values.top();
        values.pop();
        number_t b = values.top();
        values.pop();
        switch (op)
        {
            default: assert(false);
            case op_t::plus:
                values.push(b + a);
                break;
            case op_t::minus:
                values.push(b - a);
                break;
            case op_t::division:
                if (a == 0)
                {
                    throw division_by_zero_exception();
                }
                values.push(b / a);
                break;
            case op_t::multiplication:
                values.push(b * a);
                break;
        }
    }

}

void expression_t::add_operator(op_t op, std::stack<op_t>& op_stack)
{
    int p = priority(op);
    while (!op_stack.empty())
    {
        op_t top = op_stack.top();
        if (priority(top) < p)
        {
            break;
        }
        op_stack.pop();
        operations_.push_back(token_t(top));
    }
    op_stack.push(op);
}

void expression_t::parse(lexem_parser_t& input, variable_manager_t& vars)
{
    try
    {
        aux_parse(input, vars);
    }
    catch (...)
    {
        operations_.clear();
        throw;
    }
}

void expression_t::aux_parse(
        lexem_parser_t& input, variable_manager_t& vars)
{
    assert(operations_.size() == 0);
    std::stack<op_t> op_stack;
    bool expect_bin_op = false;
    while (true)
    {
        std::string const& token = input.peek();
        if (token.empty())
        {
            break;
        }
        if (isalpha(token[0]))
        {
            if (expect_bin_op)
            {
                throw parse_exception("Bad placed " + token + " variable.");
            }
            operations_.push_back(
                        token_t(token_t::variable, vars.get_var_index(token)));
            expect_bin_op = true;
        }
        else if (isdigit(token[0]))
        {
            if (expect_bin_op)
            {
                throw parse_exception("Bad placed " + aton(token)
                                      + std::string(" constant."));
            }
            operations_.push_back(token_t(token_t::constant, aton(token)));
            expect_bin_op = true;
        }
        else if (token == "+")
        {
            if (expect_bin_op)
            {
                add_operator(op_t::plus, op_stack);
                expect_bin_op = false;
            }
            // We actually don't need unary plus
        }
        else if (token == "-")
        {
            if (expect_bin_op)
            {
                add_operator(op_t::minus, op_stack);
                expect_bin_op = false;
            }
            else
            {
                add_operator(op_t::un_minus, op_stack);
            }
        }
        else if (token == "*")
        {
            if (!expect_bin_op)
            {
                throw parse_exception("Did not expect binary operator. "
                                      "Multiplication found.");
            }
            add_operator(op_t::multiplication, op_stack);
            expect_bin_op = false;
        }
        else if (token == "/")
        {
            if (!expect_bin_op)
            {
                throw parse_exception("Did not expect binary operator. "
                                      "Division found.");
            }
            add_operator(op_t::division, op_stack);
            expect_bin_op = false;
        }
        else if (token == "(")
        {
            if (expect_bin_op)
            {
                throw parse_exception("Invalid opening bracket.");
            }
            op_stack.push(op_t::opening_bracket);
            expect_bin_op = false;
        }
        else if (token == ")")
        {
            if (!expect_bin_op)
            {
                throw parse_exception("Invalid closing bracket");
            }
            bool found_matching_bracket = false;
            while (!op_stack.empty())
            {
                op_t top = op_stack.top();
                op_stack.pop();
                if (top == op_t::opening_bracket)
                {
                    found_matching_bracket = true;
                    break;
                }
                operations_.push_back(top);
            }
            if (!found_matching_bracket)
            {
                throw parse_exception("No matching bracket.");
            }
            expect_bin_op = true;

        }
        else
        {
            break;
        }
        input.next();
    }
    while (!op_stack.empty())
    {
        op_t top = op_stack.top();
        if (top == op_t::opening_bracket || top == op_t::closing_bracket)
        {
            throw parse_exception("Brackets mismatched.");
        }
        operations_.push_back(op_stack.top());
        op_stack.pop();
    }
    if (operations_.empty())
    {
        throw parse_exception("Expression expected.");
    }
}

number_t expression_t::eval(std::vector<number_t> const& vars) const
{
    std::stack<number_t> values;
    for (auto i = operations_.begin(), end = operations_.end(); i != end; ++i)
    {
        switch (i->type)
        {
            case token_t::constant:
                values.push(i->get.constant);
                break;
            case token_t::variable:
                values.push(vars[i->get.variable]);
                break;
            case token_t::operation:
                process_operation(values, i->get.operation);
                break;
        }
    }
    return values.top();
}

expression_t::token_t::token_t(type_t type, int val)
    : type(type)
{
    // Kinda no real if here(because get is a union and &constant == &variable).
    if (type == constant)
    {
        get.constant = val;
    }
    else if (type == variable)
    {
        get.variable = val;
    }
    else
    {
        assert(false);
    }
}

expression_t::token_t::token_t(operation_t op)
    : type(operation)
{
    get.operation = op;
}

parse_exception::parse_exception(const std::string &description_)
    : description_(description_)
{}

const char* parse_exception::what() const noexcept
{
    return description_.c_str();
}

parse_exception::~parse_exception() noexcept
{
}

char const* division_by_zero_exception::what() const noexcept
{
    return "division by zero.";
}

division_by_zero_exception::~division_by_zero_exception() noexcept
{}
