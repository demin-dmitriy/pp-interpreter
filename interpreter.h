#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "lexem_parser.h"
#include "operations.h"

#include <exception>
#include <iostream>
#include <vector>
#include <memory>

class syntax_error: std::exception
{
    std::string description_;
public:
    int line;
    syntax_error(int line, std::string const& description_);
    virtual char const* what() const noexcept;
    virtual ~syntax_error() noexcept;
};

class run_time_error: std::exception
{
    std::string description_;
public:
    int line;
    run_time_error(int line, std::string const& description_);
    virtual char const* what() const noexcept;
    virtual ~run_time_error() noexcept;
};

struct interpreter_t
{
    interpreter_t(std::istream& in);

    void run(std::istream& in, std::ostream& out);

private:
    std::vector<std::unique_ptr<operation_t> > operations_;
    int variable_amount_;
};

#endif // INTERPRETER_H
