CXX = g++
CXXFLAGS += -Wall
CXXFLAGS += -std=gnu++0x
RM = del
OBJECT_FILES += lexem_parser.o expression.o operations.o interpreter.o

all: CXXFLAGS += -O2
all: ADD_OBJECT_FILES += pp.o
all: pp.o
all: pp.exe

tests: lib/gtest-all.o
tests: CXXFLAGS += -DTEST_MODE -Iinclude/
tests: CXXFLAGS += -DDEBUG -g
tests: ADD_OBJECT_FILES += lib/gtest-all.o pp-tests.o
tests: pp-tests.exe
pp-tests.exe: pp-tests.o 

debug: CXXFLAGS += -DDEBUG -g
debug: ADD_OBJECT_FILES += pp.o
debug: pp.o
debug: pp.exe

clean:
	$(RM) pp-tests.exe
	$(RM) pp.exe
	$(RM) pp-tests.o
	$(RM) pp.o
	$(RM) expression.o
	$(RM) operations.o
	$(RM) lexem_parser.o

%.exe: $(OBJECT_FILES)
	$(CXX) $(OBJECT_FILES) $(ADD_OBJECT_FILES) -o $@

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $^
