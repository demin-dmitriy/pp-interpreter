#ifndef PP_H
#define PP_H

#include "interpreter.h"

#include <string>

struct arguments_t
{
    std::string script_name;
};

#endif
