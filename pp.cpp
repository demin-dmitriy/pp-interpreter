#include "pp.h"

#include <fstream>
#include <iostream>

void show_usage()
{
    std::cout << "Usage:\n    pp <script-name>\n";
}

arguments_t parse_arguments(int argc, char** argv)
{
    arguments_t res;
    if (argc != 2)
    {
        std::cout << "Invalid arguments number.\n";
        show_usage();
    }
    else
    {
        res.script_name = std::string(argv[1]);
    }
    return res;
}

#ifndef TEST_MODE

int main(int argc, char** argv)
{
    auto args = parse_arguments(argc, argv);
    if (args.script_name.empty())
    {
        return 0;
    }
    std::ifstream script_file(args.script_name);
    if (!script_file)
    {
        std::cout << "Failed to open a script\n";
        return -1;
    }
    try
    {
        interpreter_t interpreter(script_file);
        interpreter.run(std::cin, std::cout);
    }
    catch (syntax_error const& e)
    {
        std::cout << "line " << e.line + 1 << ": syntax error. "
                  << e.what() << '\n';
        return -1;
    }
    catch (run_time_error const& e)
    {
        std::cout << "line " << e.line + 1 << ": " << e.what() << '\n';
        return -2;
    }
    catch (...)
    {
        std::cout << "Unknown exception";
    }

    return 0;
}

#endif
