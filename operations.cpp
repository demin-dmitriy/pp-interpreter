#include "operations.h"

#include <cassert>

program_state_t::program_state_t(
        std::istream& in, std::ostream& out, size_t variable_count)
    : current_operation(0)
    , variables(variable_count)
    , in(in)
    , out(out)
{}

operation_t::~operation_t()
{}

void op_print_t::run(program_state_t& ps)
{
    ps.out << expression.eval(ps.variables);
}

op_print_t::~op_print_t()
{}

void op_read_t::run(program_state_t& ps)
{
    ps.in >> ps.variables[variable_index];
}

bool condition_t::eval(program_state_t& ps) const
{
    number_t a = expr_left.eval(ps.variables);
    number_t b = expr_right.eval(ps.variables);
    switch (cmp_operator)
    {
        case less:        return a < b;
        case greater:     return a > b;
        case equal:       return a == b;
        case not_less:    return a >= b;
        case not_greater: return a <= b;
        case not_equal:   return a != b;
    }
    assert(false);
    return false;
}

bool condition_t::set_cmp_operator(const std::string& cmp_op)
{
    if (cmp_op == ">")
    {
        cmp_operator = greater;
    }
    else if (cmp_op == "<")
    {
        cmp_operator = less;
    }
    else if (cmp_op == ">=")
    {
        cmp_operator = not_less;
    }
    else if (cmp_op == "<=")
    {
        cmp_operator = not_greater;
    }
    else if (cmp_op == "==")
    {
        cmp_operator = equal;
    }
    else if (cmp_op == "!=")
    {
        cmp_operator = not_equal;
    }
    else
    {
        return false;
    }
    return true;
}

void op_goto_t::run(program_state_t& ps)
{
    ps.current_operation = operation_number;
}

op_goto_t::~op_goto_t()
{}

void op_assignment_t::run(program_state_t& ps)
{
    ps.variables[variable_index] = expression.eval(ps.variables);
}

op_assignment_t::~op_assignment_t()
{}

void op_goto_if_not_t::run(program_state_t& ps)
{
    if (!condition.eval(ps))
    {
        ps.current_operation = operation_number;
    }
}

op_goto_if_not_t::~op_goto_if_not_t()
{}
