#include "gtest/gtest.h"
#include "lexem_parser.h"
#include "expression.h"
#include "interpreter.h"

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

TEST(LexemParserTest, BasicInput)
{
    istringstream test_str1("if 2* 3 +(f -h)/(4+t):#!Sample comment");
    lexem_parser_t lp1(test_str1);
    EXPECT_EQ(0, lp1.line_number());
    ASSERT_EQ("if", lp1.next());
    ASSERT_EQ("2", lp1.next());
    ASSERT_EQ("*", lp1.next());
    ASSERT_EQ("3", lp1.next());
    ASSERT_EQ("+", lp1.next());
    ASSERT_EQ("(", lp1.next());
    ASSERT_EQ("f", lp1.next());
    ASSERT_EQ("-", lp1.next());
    ASSERT_EQ("h", lp1.next());
    ASSERT_EQ(")", lp1.next());
    ASSERT_EQ("/", lp1.next());
    ASSERT_EQ("(", lp1.next());
    ASSERT_EQ("4", lp1.next());
    ASSERT_EQ("+", lp1.next());
    ASSERT_EQ("t", lp1.next());
    ASSERT_EQ(")", lp1.next());
    ASSERT_EQ(":", lp1.next());
    ASSERT_EQ("", lp1.next());
    lp1.next_line();
    EXPECT_EQ(0, lp1.line_number());
    ASSERT_EQ("", lp1.next());
    lp1.next_line();
    EXPECT_EQ(0, lp1.line_number());
    ASSERT_EQ("", lp1.next());

    istringstream test_str2("read x \n #comme#f##nt\n   #another comment\n"
                            " print 2+-  3#print\n\n");
    lexem_parser_t lp2(test_str2);
    EXPECT_EQ(0, lp2.line_number());
    ASSERT_EQ("read", lp2.next());
    ASSERT_EQ("x", lp2.next());
    ASSERT_EQ("", lp2.next());
    lp2.next_line();
    EXPECT_EQ(3, lp2.line_number());
    ASSERT_EQ("print", lp2.next());
    ASSERT_EQ("2", lp2.next());
    ASSERT_EQ("+", lp2.next());
    ASSERT_EQ("-", lp2.next());
    ASSERT_EQ("3", lp2.next());
    ASSERT_EQ("", lp2.next());
    lp2.next_line();
    EXPECT_EQ(5, lp2.line_number());
    ASSERT_EQ("", lp2.next());
    
    istringstream test_str3("");
    lexem_parser_t lp3(test_str3);
    ASSERT_EQ("", lp3.next());
    EXPECT_EQ(0, lp3.line_number());
    lp3.next_line();
    EXPECT_EQ(0, lp3.line_number());
    ASSERT_EQ("", lp3.next());
    EXPECT_EQ(0, lp3.line_number());
}

struct expr_builder
{
    istringstream expr_str;
    lexem_parser_t lp;
    variable_manager_t vm;
    vector<int> variables;
    expression_t expr;

    expr_builder(string const& str)
        : expr_str(str)
        , lp(expr_str)
    {
        expr.parse(lp, vm);
        variables.resize(vm.amount(), 0);
    }

    void set(string str, number_t value)
    {
        size_t index = vm.get_var_index(str);
        if (index < variables.size())
        {
            variables[index] = value;
        }
    }

    number_t eval()
    {
        return expr.eval(variables);
    }
};

TEST(ExpressionTest, BasicEvaluations)
{
    expr_builder expr1("2 +3");
    EXPECT_EQ(5, expr1.eval());
    EXPECT_EQ((size_t)0, expr1.variables.size());
    expr_builder expr2("12 + -4 / -7 + (12 + 3 ) *4");
    EXPECT_EQ(72, expr2.eval());
    expr_builder expr3("a + b * c - 1");
    EXPECT_EQ((size_t)3, expr3.variables.size());
    EXPECT_EQ(-1, expr3.eval());
    expr3.set("a", 10);
    expr3.set("b", 5);
    expr3.set("c", 5);
    EXPECT_EQ(34, expr3.eval());
    expr_builder expr4("(((3 +++ -((+4))))*2)");
    EXPECT_EQ(-2, expr4.eval());
    expr_builder expr5("ta12");
    EXPECT_EQ(0, expr5.eval());
    expr5.set("ta12", 17);
    EXPECT_EQ(17, expr5.eval());
}

TEST(ExpressionTest, IllTypedExpressions)
{
    EXPECT_THROW(expr_builder expr(""), parse_exception);
    EXPECT_THROW(expr_builder expr("(2 + 3"), parse_exception);
    EXPECT_THROW(expr_builder expr("2 + 3)"), parse_exception);
    EXPECT_THROW(expr_builder expr("2 ( 3  + 5)"), parse_exception);
    EXPECT_THROW(expr_builder expr("2+*3"), parse_exception);
    EXPECT_THROW(expr_builder expr("a-*b"), parse_exception);
    EXPECT_THROW(expr_builder expr("12a"), parse_exception);
}

TEST(ExpressionTest, ZeroDivision)
{
    expr_builder expr("a / b");
    EXPECT_THROW(expr.eval(), division_by_zero_exception);
    expr.set("a", 1);
    EXPECT_THROW(expr.eval(), division_by_zero_exception);
    expr.set("b", 2);
    EXPECT_NO_THROW(expr.eval());
}

TEST(InterpreterTest, ExampleRootCountProgram)
{
    istringstream prog_text(
                "# rootN.pp\n"
                "# ��������� ���������� ������ ����������� ���������\n"
                "read a\n"
                "read b\n"
                "read c\n"
                "\n"
                "if a != 0:\n"
                "	d = b * b - 4 * a * c\n"
                "	if d > 0:\n"
                "		print 2\n"
                "	end\n"
                "	if d == 0:\n"
                "		print 1\n"
                "	end\n"
                "	if d < 0:\n"
                "		print 0\n"
                "	end\n"
                "end\n"
                "\n"
                "if a == 0:\n"
                "	if b == 0:\n"
                "		if c == 0:\n"
                "			print -1 # ��� ����������� �������������\n"
                "		end\n"
                "		if c != 0:\n"
                "			print 0\n"
                "		end\n"
                "	end\n"
                "	if b != 0:\n"
                "		print 1\n"
                "	end\n"
                "end\n"
                );
    interpreter_t prog(prog_text);
    istringstream input1("1 2 1");
    ostringstream output1;
    prog.run(input1, output1);
    EXPECT_EQ("1", output1.str());
    istringstream input2("0 0 0");
    ostringstream output2;
    prog.run(input2, output2);
    EXPECT_EQ("-1", output2.str());
    istringstream input3("5 4 3");
    ostringstream output3;
    prog.run(input3, output3);
    EXPECT_EQ("0", output3.str());
    istringstream input4("1 1 -1");
    ostringstream output4;
    prog.run(input4, output4);
    EXPECT_EQ("2", output4.str());
}

TEST(InterpreterTest, ExampleFibProgram)
{
    istringstream prog_text(
                "# fib.pp\n"
                "# ���������� n-���� ����� ��������� �� PP\n"
                "read n\n"
                "a = 1\n"
                "b = 1\n"
                "while n > 1:\n"
                "	t = b\n"
                "	b = b + a\n"
                "	a = t\n"
                "	n = n - 1\n"
                "end\n"
                "print a");
    interpreter_t prog(prog_text);
    istringstream input1("300");
    ostringstream output1;
    prog.run(input1, output1);
    EXPECT_EQ("-1287770608", output1.str());
    istringstream input2("1");
    ostringstream output2;
    prog.run(input2, output2);
    EXPECT_EQ("1", output2.str());
}

TEST(InterpreterTest, ExamplePrimeNProgram)
{
    istringstream prog_text(
                "# primeN.pp\n"
                "# ��������� N ������ ������� �����\n"
                "\n"
                "read N\n"
                "\n"
                "count = 0\n"
                "i = 1\n"
                "while count != N:\n"
                "	i = i + 1\n"
                "	j = 1\n"
                "	condition1 = 1\n"
                "	while condition1 == 1:\n"
                "		j = j + 1\n"
                "		if (i / j) * j == i:\n"
                "			condition1 = 0\n"
                "		end\n"
                "		if j == i:\n"
                "			condition1 = 2\n"
                "		end\n"
                "	end\n"
                "	if condition1 == 2:\n"
                "		print -10 # ����������� ����� �������\n"
                "		print i\n"
                "       count = count + 1\n"
                "	end\n"
                "end");
    interpreter_t prog(prog_text);
    istringstream input1("5");
    ostringstream output1;
    prog.run(input1, output1);
    EXPECT_EQ("-102-103-105-107-1011", output1.str());
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
