#include "interpreter.h"

#include <cctype>
#include <stack>

using namespace std;

namespace
{
    enum flow_t
    {
        if_branch,
        while_loop
    };

    struct control_flow
    {
        flow_t type;
        int index;
        control_flow(flow_t type, int index)
            : type(type)
            , index(index)
        {}
    };

    unique_ptr<op_goto_if_not_t> read_cmp_statement(
            lexem_parser_t& parser,
            variable_manager_t& variables
            )
    {
        unique_ptr<op_goto_if_not_t> new_op(new op_goto_if_not_t());
        new_op->condition.expr_left.parse(parser, variables);
        string cmp_op = parser.next();
        if (!new_op->condition.set_cmp_operator(cmp_op))
        {
            throw syntax_error(
                        parser.line_number(),
                        "Unknow compare operator in compare expression.");
        }
        new_op->condition.expr_right.parse(parser, variables);
        if (parser.next() != ":")
        {
            throw syntax_error(
                        parser.line_number(),
                        "Expected ':' at the end of compare expression.");
        }
        return new_op;
    }
}

interpreter_t::interpreter_t(std::istream &in)
{
    using namespace std;
    lexem_parser_t parser(in);
    variable_manager_t variables;
    vector<control_flow> controls;

    while (true)
    {
        string str = parser.next();
        if (str.empty())
        {
            break;
        }
        if (str == "print")
        {
            unique_ptr<op_print_t> new_op(new op_print_t());
            new_op->expression.parse(parser, variables);
            operations_.push_back(unique_ptr<operation_t>(new_op.release()));
        }
        else if (str == "read")
        {
            string str = parser.next();
            if (str.empty() || !isalpha(str[0]))
            {
                throw syntax_error(parser.line_number(),
                                   "Expected a variable as argument for read.");
            }
            unique_ptr<op_read_t> new_op(new op_read_t);
            new_op->variable_index = variables.get_var_index(str);
            operations_.push_back(unique_ptr<operation_t>(new_op.release()));
        }
        else if (str == "if")
        {
            unique_ptr<op_goto_if_not_t> new_op
                    = read_cmp_statement(parser, variables);
            controls.push_back(control_flow(if_branch, operations_.size()));
            operations_.push_back(unique_ptr<operation_t>(new_op.release()));
        }
        else if (str == "while")
        {
            unique_ptr<op_goto_if_not_t> new_op
                    = read_cmp_statement(parser, variables);
            controls.push_back(control_flow(while_loop, operations_.size()));
            operations_.push_back(unique_ptr<operation_t>(new_op.release()));
        }
        else if (str == "end")
        {
            if (controls.empty())
            {
                throw syntax_error(parser.line_number(),
                                   "Unexpected end here. ");
            }
            if (controls.back().type == while_loop)
            {
                unique_ptr<op_goto_t> new_op(new op_goto_t());
                new_op->operation_number = controls.back().index - 1;
                operations_.push_back(unique_ptr<operation_t>(new_op.release()));
            }
            dynamic_cast<op_goto_if_not_t*>(
                        operations_[controls.back().index].get())
                            ->operation_number = operations_.size() - 1;
            controls.pop_back();
        }
        else if (isalpha(str[0]))
        {
            if (parser.next() != "=")
            {
                throw syntax_error(
                            parser.line_number(), "Expected assignment.");
            }
            unique_ptr<op_assignment_t> new_op(
                        new op_assignment_t());
            new_op->variable_index = variables.get_var_index(str);
            new_op->expression.parse(parser, variables);
            operations_.push_back(unique_ptr<operation_t>(new_op.release()));
        }
        else
        {
            throw syntax_error(
                        parser.line_number(), "Unexpected token " + str + ".");
        }
        str = parser.next();
        if (!str.empty())
        {
            throw syntax_error(parser.line_number(),
                               "Expected end of line. " + str + "found.");
        }
        operations_.back()->line_number = parser.line_number();
        parser.next_line();
    }
    variable_amount_ = variables.amount();
}

void interpreter_t::run(std::istream &in, std::ostream &out)
{
    program_state_t program_state(in, out, variable_amount_);
    size_t& line = program_state.current_operation;
    try
    {
        for (size_t end = operations_.size(); line != end; ++line)
        {
            operations_[line]->run(program_state);
        }
    }
    catch (division_by_zero_exception const& e)
    {
        throw run_time_error(operations_[line]->line_number, e.what());
    }
}

syntax_error::syntax_error(int line, std::string const& description)
    : description_(description)
    , line(line)
{}

char const* syntax_error::what() const noexcept
{
    return description_.c_str();
}

syntax_error::~syntax_error() noexcept
{}

run_time_error::run_time_error(int line, std::string const& description)
    : description_(description)
    , line(line)
{}

char const* run_time_error::what() const noexcept
{
    return description_.c_str();
}

run_time_error::~run_time_error() noexcept
{}

