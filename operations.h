#ifndef OPERATIONS_H
#define OPERATIONS_H

#include "expression.h"

#include <vector>
#include <string>

struct program_state_t
{
    size_t current_operation;
    std::vector<number_t> variables;
    std::istream& in;
    std::ostream& out;
    program_state_t(std::istream& in, std::ostream& out, size_t variable_count);
};

struct operation_t
{
    int line_number;
    virtual void run(program_state_t&) = 0;
    virtual ~operation_t() = 0;
};

struct op_print_t: operation_t
{
    expression_t expression;
    virtual void run(program_state_t&);
    virtual ~op_print_t();
};

struct op_read_t: operation_t
{
    size_t variable_index;
    virtual void run(program_state_t&);
};

struct condition_t
{
    expression_t expr_left;
    expression_t expr_right;
    enum
    {
        less,
        greater,
        equal,
        not_less,
        not_greater,
        not_equal
    } cmp_operator;

    bool eval(program_state_t&) const;
    bool set_cmp_operator(std::string const&);
};

struct op_goto_t: operation_t
{
    int operation_number;
    virtual void run(program_state_t&);
    virtual ~op_goto_t();
};

struct op_goto_if_not_t: operation_t
{
    int operation_number;
    condition_t condition;
    virtual void run(program_state_t&);
    virtual ~op_goto_if_not_t();
};

struct op_assignment_t:operation_t
{
    size_t variable_index;
    expression_t expression;
    virtual void run(program_state_t&);
    virtual ~op_assignment_t();
};

#endif // OPERATIONS_H
